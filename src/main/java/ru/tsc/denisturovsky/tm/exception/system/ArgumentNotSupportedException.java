package ru.tsc.denisturovsky.tm.exception.system;

import ru.tsc.denisturovsky.tm.exception.AbstractException;

public final class ArgumentNotSupportedException extends AbstractException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Error! Command ``" + argument + "`` not supported...");
    }

}
