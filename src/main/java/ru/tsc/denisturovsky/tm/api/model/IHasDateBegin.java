package ru.tsc.denisturovsky.tm.api.model;

import java.util.Date;

public interface IHasDateBegin {

    Date getDateBegin();

    void setDateBegin(Date dateBegin);

}
